CREATE TABLE `Pessoa` (
	`Nome` TEXT(50) NOT NULL,
	`E-mail` TEXT(40) NOT NULL AUTO_INCREMENT,
	`Codigo_pessoa` int(4) NOT NULL,
	`Senha` varchar(10) NOT NULL,
	PRIMARY KEY (`E-mail`)
);

CREATE TABLE `Instituicao` (
	`Codigo_instituicao` int(4) NOT NULL AUTO_INCREMENT,
	`Nome` TEXT(50) NOT NULL,
	PRIMARY KEY (`Codigo_instituicao`)
);

CREATE TABLE `Aluno` (
	`Codigo_aluno` int(4) NOT NULL AUTO_INCREMENT,
	`Codigo_instituicao_aluno` int(4) NOT NULL,
	`Codigo_turmas_aluno` int(4) NOT NULL,
	PRIMARY KEY (`Codigo_aluno`)
);

CREATE TABLE `Professor` (
	`Codigo_professor` int(4) NOT NULL AUTO_INCREMENT,
	`Codigo_instituicao_professor` int(4) NOT NULL,
	`Codigo_area` int(4) NOT NULL,
	PRIMARY KEY (`Codigo_professor`)
);

CREATE TABLE `Disciplinas` (
	`Codigo_disciplina` int(4) NOT NULL AUTO_INCREMENT,
	`Nome` TEXT(40) NOT NULL,
	PRIMARY KEY (`Codigo_disciplina`)
);

CREATE TABLE `Professor_disciplinas` (
	`Codigo_disciplina` int(4) NOT NULL,
	`Codigo_professor` int(4) NOT NULL
);

CREATE TABLE `Areas` (
	`Codigo_area` int(4) NOT NULL AUTO_INCREMENT,
	`Nome` TEXT(40) NOT NULL,
	PRIMARY KEY (`Codigo_area`)
);

CREATE TABLE `Areas_disciplinas` (
	`Codigo_areas` int(4) NOT NULL,
	`Codigo_disciplinas` int(4) NOT NULL
);

CREATE TABLE `Turmas` (
	`Codigo_turma` int(4) NOT NULL AUTO_INCREMENT,
	`Nome` TEXT(40) NOT NULL,
	PRIMARY KEY (`Codigo_turma`)
);

CREATE TABLE `Instituicao_turmas` (
	`Codigo_instituicao` int(4) NOT NULL,
	`Codigo_turmas` int(4) NOT NULL
);

CREATE TABLE `Turmas_disciplinas` (
	`Codigo_turmas` int(4) NOT NULL,
	`Codigo_disciplinas` int(4) NOT NULL
);

CREATE TABLE `Instituicao_professor` (
	`Codigo_instituicao` int(4) NOT NULL,
	`Codigo_professor` int(4) NOT NULL
);

CREATE TABLE `Desafio` (
	`Codigo_desafio` int(4) NOT NULL AUTO_INCREMENT,
	`Nome` TEXT(40) NOT NULL,
	`Valor` int(3) NOT NULL,
	PRIMARY KEY (`Codigo_desafio`)
);

CREATE TABLE `Questoes` (
	`Codigo_questao` int(4) NOT NULL AUTO_INCREMENT,
	`Alternativa1` TEXT(50) NOT NULL,
	`Alternativa2` TEXT(50) NOT NULL,
	`Alternativa3` TEXT(50) NOT NULL,
	`Alternativa4` TEXT(50) NOT NULL,
	`Codigo_area` int(4) NOT NULL,
	`Codigo_nivel` int NOT NULL,
	`Texto_questao` TEXT(300) NOT NULL,
	PRIMARY KEY (`Codigo_questao`)
);

CREATE TABLE `Desafio_questoes` (
	`Codigo_desafio` int(4) NOT NULL,
	`Codigo_questao` int(4) NOT NULL
);

CREATE TABLE `Professor_desafio` (
	`Codigo_professor` int(4) NOT NULL,
	`Codigo_desafio` int(4) NOT NULL
);

CREATE TABLE `Turmas_desafio` (
	`Codigo_turma` int(4) NOT NULL,
	`Codigo_desafio` int(4) NOT NULL
);

CREATE TABLE `Nivel` (
	`Codigo_nivel` int(4) NOT NULL AUTO_INCREMENT,
	`Nivel` TEXT(10) NOT NULL,
	PRIMARY KEY (`Codigo_nivel`)
);

CREATE TABLE `Aluno_desafio` (
	`Codigo_aluno` int(4) NOT NULL,
	`Codigo_desafio` int(4) NOT NULL,
	`Pontuacao` int(4) NOT NULL
);

ALTER TABLE `Aluno` ADD CONSTRAINT `Aluno_fk0` FOREIGN KEY (`Codigo_aluno`) REFERENCES `Pessoa`(`Codigo_pessoa`);

ALTER TABLE `Aluno` ADD CONSTRAINT `Aluno_fk1` FOREIGN KEY (`Codigo_instituicao_aluno`) REFERENCES `Instituicao`(`Codigo_instituicao`);

ALTER TABLE `Aluno` ADD CONSTRAINT `Aluno_fk2` FOREIGN KEY (`Codigo_turmas_aluno`) REFERENCES `Turmas`(`Codigo_turma`);

ALTER TABLE `Professor` ADD CONSTRAINT `Professor_fk0` FOREIGN KEY (`Codigo_professor`) REFERENCES `Pessoa`(`Codigo_pessoa`);

ALTER TABLE `Professor` ADD CONSTRAINT `Professor_fk1` FOREIGN KEY (`Codigo_instituicao_professor`) REFERENCES `Instituicao`(`Codigo_instituicao`);

ALTER TABLE `Professor` ADD CONSTRAINT `Professor_fk2` FOREIGN KEY (`Codigo_area`) REFERENCES `Areas`(`Codigo_area`);

ALTER TABLE `Professor_disciplinas` ADD CONSTRAINT `Professor_disciplinas_fk0` FOREIGN KEY (`Codigo_disciplina`) REFERENCES `Disciplinas`(`Codigo_disciplina`);

ALTER TABLE `Professor_disciplinas` ADD CONSTRAINT `Professor_disciplinas_fk1` FOREIGN KEY (`Codigo_professor`) REFERENCES `Professor`(`Codigo_professor`);

ALTER TABLE `Areas_disciplinas` ADD CONSTRAINT `Areas_disciplinas_fk0` FOREIGN KEY (`Codigo_areas`) REFERENCES `Areas`(`Codigo_area`);

ALTER TABLE `Areas_disciplinas` ADD CONSTRAINT `Areas_disciplinas_fk1` FOREIGN KEY (`Codigo_disciplinas`) REFERENCES `Disciplinas`(`Codigo_disciplina`);

ALTER TABLE `Instituicao_turmas` ADD CONSTRAINT `Instituicao_turmas_fk0` FOREIGN KEY (`Codigo_instituicao`) REFERENCES `Instituicao`(`Codigo_instituicao`);

ALTER TABLE `Instituicao_turmas` ADD CONSTRAINT `Instituicao_turmas_fk1` FOREIGN KEY (`Codigo_turmas`) REFERENCES `Turmas`(`Codigo_turma`);

ALTER TABLE `Turmas_disciplinas` ADD CONSTRAINT `Turmas_disciplinas_fk0` FOREIGN KEY (`Codigo_turmas`) REFERENCES `Turmas`(`Codigo_turma`);

ALTER TABLE `Turmas_disciplinas` ADD CONSTRAINT `Turmas_disciplinas_fk1` FOREIGN KEY (`Codigo_disciplinas`) REFERENCES `Disciplinas`(`Codigo_disciplina`);

ALTER TABLE `Instituicao_professor` ADD CONSTRAINT `Instituicao_professor_fk0` FOREIGN KEY (`Codigo_instituicao`) REFERENCES `Instituicao`(`Codigo_instituicao`);

ALTER TABLE `Instituicao_professor` ADD CONSTRAINT `Instituicao_professor_fk1` FOREIGN KEY (`Codigo_professor`) REFERENCES `Professor`(`Codigo_professor`);

ALTER TABLE `Questoes` ADD CONSTRAINT `Questoes_fk0` FOREIGN KEY (`Codigo_area`) REFERENCES `Areas`(`Codigo_area`);

ALTER TABLE `Questoes` ADD CONSTRAINT `Questoes_fk1` FOREIGN KEY (`Codigo_nivel`) REFERENCES `Nivel`(`Codigo_nivel`);

ALTER TABLE `Desafio_questoes` ADD CONSTRAINT `Desafio_questoes_fk0` FOREIGN KEY (`Codigo_desafio`) REFERENCES `Desafio`(`Codigo_desafio`);

ALTER TABLE `Desafio_questoes` ADD CONSTRAINT `Desafio_questoes_fk1` FOREIGN KEY (`Codigo_questao`) REFERENCES `Questoes`(`Codigo_questao`);

ALTER TABLE `Professor_desafio` ADD CONSTRAINT `Professor_desafio_fk0` FOREIGN KEY (`Codigo_professor`) REFERENCES `Professor`(`Codigo_professor`);

ALTER TABLE `Professor_desafio` ADD CONSTRAINT `Professor_desafio_fk1` FOREIGN KEY (`Codigo_desafio`) REFERENCES `Desafio`(`Codigo_desafio`);

ALTER TABLE `Turmas_desafio` ADD CONSTRAINT `Turmas_desafio_fk0` FOREIGN KEY (`Codigo_turma`) REFERENCES `Turmas`(`Codigo_turma`);

ALTER TABLE `Turmas_desafio` ADD CONSTRAINT `Turmas_desafio_fk1` FOREIGN KEY (`Codigo_desafio`) REFERENCES `Desafio`(`Codigo_desafio`);

ALTER TABLE `Aluno_desafio` ADD CONSTRAINT `Aluno_desafio_fk0` FOREIGN KEY (`Codigo_aluno`) REFERENCES `Aluno`(`Codigo_aluno`);

ALTER TABLE `Aluno_desafio` ADD CONSTRAINT `Aluno_desafio_fk1` FOREIGN KEY (`Codigo_desafio`) REFERENCES `Desafio`(`Codigo_desafio`);

