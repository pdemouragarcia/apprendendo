/*###############################################################################################*/
//FUNÇÕES AUXILIARES
function Carrega() //DE 5 EM 5 SEGUNDOS VERIFICA A CONEXÃO DA INTERNET
{
    setInterval(Checa_conexao,5000);
}




function auxiliar()
{
         var valores = new Array();
            $("input[name='checkbox-disciplinas[]']:checked").each(function ()
                {
             
                valores.push( $(this).val());
               
                });
    return (valores);
}





function Checa_conexao ()
{
          
  
     if (navigator.network.connection.type == Connection.NONE)
        {
         $("*#conexao").attr("src","images/offline.png");  
         return 0;
           
            
        }
    else
    {
        $("*#conexao").attr("src","images/online.png");
        return 1;
    }    
    
    
}


function Sair_app()
{
    alert ("Obrigado por utilizar o APPrendendo!");
    navigator.app.exitApp();
}



/*###############################################################################################*/
//FUNÇÕES DE SOLICITAÇÕES AO BANCO USANDO AJAX
function Ajax_busca_areas(tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
         
           requisicao: tipo_requisicao,
           
         }, 
         context: jQuery("#select-areas"),  
         success : function (retorno_areas)
          {
        
              this.append(retorno_areas);
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_busca_disciplinas(area,tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
           area: area,
           requisicao: tipo_requisicao,
           
         }, 
         context: jQuery("#checkbox-disciplinas"),  
         success : function (retorno_disciplinas)
          {
    
              this.append(retorno_disciplinas);
          
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_cadastra_areas_disciplinas (tipo_cadastro,email_cadastro,area,selecionados, parametro)
{
    $.ajax({
      url: "http://apprendendo.esy.es/api.php",  
      type : 'POST',
          
        data: {
           tipos:tipo_cadastro,
           email:email_cadastro,   
           area_professor: area,
           valores: JSON.stringify(selecionados),
           requisicao: parametro,
            
        },
        success : function (retorno_cadastro)
        {
            alert ("Parabéns, cadastro completo!");
            $(location).attr("href", "#inicio");
            
        },
        error: function()
        {
            alert("Erro na função");
        }
        
    });
}



/*###############################################################################################*/
//FUNÇÃO DE INICIALIZAÇÃO E MONITORAMENTO DOS BOTÕES DO HTML
window.onload = function ()
{

    
    
    Checa_conexao (); //Executa apenas na primeira vez no Onload da página
    Carrega();
    
    
        
    $('option', '#select-areas').remove();
    $('input','#checkbox-disciplinas').remove();
    $('label','#checkbox-disciplinas').remove();
    $('legend','#checkbox-disciplinas').remove();
    Ajax_busca_areas("4");   
        
    
    function monitora()
    {
         $(document).on("pagehide","tela_efetuar_cadastro_areadisciplina.html",function(evt)
        {
           
        });
    
    
    
        $(document).on("change","#select-areas",function (evt) //Está retornando quando seleciona
        {
       
                
                localStorage.setItem('area',$("#select-areas").val());
                Ajax_busca_disciplinas(localStorage.getItem('area'),"5"); 
    
        
        });
    
    
    
   
        $(document).on("click","#btn_cadastrar_areadisciplina", function(evt)
                {

                        selecionados = auxiliar(); 
                        Ajax_cadastra_areas_disciplinas(localStorage.getItem('tipo'), localStorage.getItem('email'),localStorage.getItem('area'),selecionados, "6");
                        //tipo_usuario e email_usuario vai ter que arrumar
        
                });
    
    
        $(document).on("click", "#btn_ir_inicio", function(evt)
            {
                $(location).attr("href", "tela_inicio.html");
                //Só pode ir para o início se a sessão já estiver ativa né ? Por hora deixa assim.

            });
    
        $(document).on("click", "#btn_voltar_pagina", function(evt)
            {
    
                window.history.back();

            });
    
        $(document).on("click", "#btn_avancar_pagina", function(evt)
            {
      
                window.history.forward();

            });
    
    
    
        $(document).on("click", "#btn_sair_aplicativo", function(evt) //CLIC PARA SAIR DO APP
            {
                Sair_app();
                localStorage.clear();
            });
    }
    window.addEventListener("pageshow",monitora,false);  
   
    

 
    
}