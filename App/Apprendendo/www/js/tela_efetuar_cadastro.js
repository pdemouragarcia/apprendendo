/*###############################################################################################*/
//FUNÇÕES AUXILIARES
function Carrega() //DE 5 EM 5 SEGUNDOS VERIFICA A CONEXÃO DA INTERNET
{
    setInterval(Checa_conexao,5000);
}



function Checa_conexao ()
{
          
  
     if (navigator.network.connection.type == Connection.NONE)
        {
         $("*#conexao").attr("src","images/offline.png");  
         return 0;
           
            
        }
    else
    {
        $("*#conexao").attr("src","images/online.png");
        return 1;
    }    
    
    
}


function auxiliar()
{
         var valores = new Array();
            $("input[name='checkbox-disciplinas[]']:checked").each(function ()
                {
             
                valores.push( $(this).val());
               
                });
    return (valores);
}





function Sair_app()
{
    alert ("Obrigado por utilizar o APPrendendo!");
    navigator.app.exitApp();
}




/*###############################################################################################*/
//FUNÇÕES DE SOLICITAÇÕES AO BANCO USANDO AJAX
function Ajax_cadastro(valores, tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'POST',  
        dataType: "json",
         data : {   
           email: valores[0],
           senha: valores[1],
           nome: valores[2],
           tipo: valores[3],
           acesso: valores[4],
           escola: valores[5],   
           requisicao: tipo_requisicao,
             
         }, 
         success : function (retorno_cadastro)
          {
                    alert ("Cadastro efetuado com sucesso!");//Mudar essa mensagem para algo no padrão de interface
                    $(location).attr("href", "tela_inicio.html");
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}





function Ajax_escolas(tipo_requisicao)
{

    $.ajax({   
        //url: "http://localhost/api.php",    
        url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
             requisicao: tipo_requisicao,
         },  
         context: jQuery("#select-choice-7"),
         success : function (retorno_escolas)
        
          {
           
              this.append(retorno_escolas);

          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_busca_areas(tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
         
           requisicao: tipo_requisicao,
           
         }, 
         context: jQuery("#select-areas"),  
         success : function (retorno_areas)
          {
        
              this.append(retorno_areas);
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_busca_disciplinas(area,tipo_requisicao)
{
    $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'GET',   
         data : {   
           area: area,
           requisicao: tipo_requisicao,
           
         }, 
         context: jQuery("#checkbox-disciplinas"),  
         success : function (retorno_disciplinas)
          {
    
              this.append(retorno_disciplinas);
          
                    
          },
        error: function ()
                {
                    alert ("Erro na função");
                }
       
       });   
}



function Ajax_cadastra_areas_disciplinas (tipo_cadastro,email_cadastro,area,selecionados, parametro)
{
    $.ajax({
      url: "http://apprendendo.esy.es/api.php",  
      type : 'POST',
          
        data: {
           tipos:tipo_cadastro,
           email:email_cadastro,   
           area_professor: area,
           valores: JSON.stringify(selecionados),
           requisicao: parametro,
            
        },
        success : function (retorno_cadastro)
        {
            alert ("Parabéns, cadastro completo!");
            $(location).attr("href", "tela_inicio.html");
            
        },
        error: function()
        {
            alert("Erro na função");
        }
        
    });
}


/*###############################################################################################*/
//FUNÇÃO DE INICIALIZAÇÃO E MONITORAMENTO DOS BOTÕES DO HTML
window.onload = function ()
{
    var email = localStorage.getItem('email');   
    var senha = localStorage.getItem('senha');
    
    
    Checa_conexao (); //Executa apenas na primeira vez no Onload da página
    Carrega();
    Ajax_escolas("2");
    
    
    function monitora()
    {
        
        $(document).on("click", "#btn_efetuar_cadastro", function(evt)
        {

            var tipo_cadastro;
  
         
            if ($("#ipt_professor").is(":checked"))
                {
                    tipo_cadastro = "1"; //Representa que é um professor
                }
            else
                {
                    if ($("#ipt_estudante").is(":checked"))
                     {
                         tipo_cadastro = "2";
                     }
                    else
                    {
                     //Aviso que o usuário esqueceu de selecionar se é professor ou aluno
                    }
                }
         
            var valores = [
          
                email,
                senha,
                $("#ipt_nome").val(),
                tipo_cadastro,
                "1",//Seta para 1 campo de usuário cadastrado
                $("#select-choice-7").val()
                //xx 
                        ];

            Ajax_cadastro(valores,"3"); 
        //alert ("Conteúdo de valores é:"+valores);

        });
    
    
        $(document).on("click", "#btn_ir_inicio", function(evt)
        {
            $(location).attr("href", "tela_inicio.html");
        //Só pode ir para o início se a sessão já estiver ativa né ? Por hora deixa assim.

        });
    
    
        $(document).on("click", "#btn_voltar_pagina", function(evt)
        {

            window.history.back();

        });
    
    
        $(document).on("click", "#btn_avancar_pagina", function(evt)
        {
      
            window.history.forward();

        });
    
    
        $(document).on("click", "#btn_sair_aplicativo", function(evt) //CLIC PARA SAIR DO APP
        {
            Sair_app();
        });
        
    }
    window.addEventListener("pageshow",monitora,false);
    
    

}