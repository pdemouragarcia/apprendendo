/*###############################################################################################*/
//FUNÇÕES AUXILIARES

function Carrega() //DE 5 EM 5 SEGUNDOS VERIFICA A CONEXÃO DA INTERNET
{
    setInterval(Checa_conexao,5000);
}



function Checa_conexao ()
{
          
  
     if (navigator.network.connection.type == Connection.NONE)
        {
         $("*#conexao").attr("src","images/offline.png");  
         return 0;
           
            
        }
    else
    {
        $("*#conexao").attr("src","images/online.png");
        return 1;
    }    
    
    
}


function Faz_login(email,senha) 
{
    
    if (Checa_conexao() == 1)
        {
            /*Login pelo banco do back end*/
          
            Ajax_login(email,senha,"1"); //Requisição tipo 1 significa que é uma requisição de Autenticação
          
        
        }
    else
        {
            /*Login pelo banco front end*/
        }
    
}



function Sair_app()
{
    alert ("Obrigado por utilizar o APPrendendo!");
    navigator.app.exitApp();
}


/*###############################################################################################*/
//FUNÇÕES DE SOLICITAÇÕES AO BANCO USANDO AJAX
function Ajax_login (email,senha,tipo_requisicao)
{
  
        $.ajax({   
         //url: "http://localhost/api.php",   
         url: "http://apprendendo.esy.es/api.php",   
         type : 'POST',   
         dataType: "json",
         data : {   
           email: email,   
           senha: senha,   
           requisicao: tipo_requisicao,
         },   
    
         success : function (retorno_login)
          {
                
             if(retorno_login[2] == "1")
                 {
                       tipo_usuario = retorno_login[1];
                       email_usuario = retorno_login[0];
                       
                       localStorage.setItem('tipo',tipo_usuario);  
                     
                       $(location).attr("href", "tela_inicio.html"); //Login efetuado com sucesso
                       $("#login_aviso").hide();
                       $("#botao_novocadastro").hide();
                 }
              else
                  {
                      if (retorno_login[2] == "2") //Provável erro de senha
                          {
                            
                              $("#ipt_senha").css("border", "1px solid #FF6347");
                              $("#ipt_senha").css("background", "1px solid #FFB6C1");
                              $("#login_mensagem").text("Olá, o e-mail está certo mas a senha pode estar errada. Tente novamente.");  
                              $("#icone_mensagem").attr("src","images/icone_atencao.png");
                              $("#login_aviso").show();
                              localStorage.clear(); //Limpa localstorage pois senha e email são inválidos
                              
                          }
                      else //Provável primeiro acesso
                          {
                              
                              $("#icone_mensagem").attr("src","images/icone_duvida.png");
                              $("#login_mensagem").text("Olá, é o seu primeiro acesso? Se sim, cadastre-se clicando no botão abaixo.");  
                              $("#login_aviso").show();
                              $("#botao_novocadastro").show();
                                
                          }
                  } 
            
         },
        error: function ()
                {
                    alert ("Erro na função aqui");
                }
       
       });     

}


/*###############################################################################################*/
//FUNÇÃO DE INICIALIZAÇÃO E MONITORAMENTO DOS BOTÕES DO HTML
window.onload = function ()

{
    
    if( navigator.splashscreen && navigator.splashscreen.hide ) // Cordova API detected //TRECHO APENAS PARA ESTA PÁGINA
    {   
        navigator.splashscreen.hide() ;
    }
    
    
    Checa_conexao (); //Executa apenas na primeira vez no Onload da página
    Carrega();
    
    
    function monitora ()
    {
        
         $(document).on("click", "#btn_efetuar_login", function(evt) //CLIC NO BOTÃO ENTRAR
    {
        
        var email = $("#ipt_email").val();
        var senha = $("#ipt_senha").val(); 
        
        localStorage.setItem ('email', email);
        localStorage.setItem ('senha', senha);
       
        Faz_login(email,senha);
        
        
        
    });
    
    
    $(document).on("click", "#btn_efetuar_novocadastro", function(evt)
    {
        $(location).attr("href", "tela_efetuar_cadastro.html");
        $("#login_aviso").hide();
        $("#botao_novocadastro").hide();
    });
    
    
    $(document).on("click", "#btn_recuperar_senha", function(evt) //CLIC BOTÃO ESQUECI SENHA
    {
         $(location).attr("href", "tela_recuperar_senha.html");
    });
    
    
 
    
    $(document).on("click", "#btn_sair_aplicativo", function(evt) //CLIC PARA SAIR DO APP
    {
        Sair_app();
        localStorage.clear();
    });
        
    }
    window.addEventListener("pageshow",monitora,false);
    
  

}












